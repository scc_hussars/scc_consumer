package com.capgemini.scc.consumer.contract.car;

import com.capgemini.scc.consumer.SccConsumerApplication;
import com.capgemini.scc.consumer.model.Car;
import com.capgemini.scc.consumer.model.Color;
import com.capgemini.scc.consumer.repository.CarRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(ids = {"com.capgemini.scc:scc_producer:+:stubs:8095"}, stubsMode = StubRunnerProperties.StubsMode.LOCAL)
@SpringBootTest(classes = SccConsumerApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@DirtiesContext
public class CarRepositoryContractTest {

    @Autowired
    private CarRepository carRepository;

    @Test
    public void whenGettingCarOtherThanWhite_expectFilledList() {
        verifyCorrectness(Color.RED);
        verifyCorrectness(Color.GREEN);
        verifyCorrectness(Color.BLUE);
        verifyCorrectness(Color.BLACK);
    }

    private void verifyCorrectness(Color color) {
        List<Car> carsWithSpecificColor = carRepository.getCarsWithColor(color);
        assertThat(carsWithSpecificColor).isNotEmpty();
        assertThat(carsWithSpecificColor.get(0).getColor()).isEqualTo(color);
    }

    @Test
    public void whenGettingCarOtherThanWhite_expectException() {
        try {
            carRepository.getCarsWithColor(Color.WHITE);
        } catch (HttpStatusCodeException e) {
            assertThat(e.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        }
    }
}
