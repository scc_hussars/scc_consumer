package com.capgemini.scc.consumer.repository;

import com.capgemini.scc.consumer.model.Car;
import com.capgemini.scc.consumer.model.Color;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class CarRepository {

    private RestTemplate carRestTemplate;

    public CarRepository(RestTemplate carRestTemplate) {
        this.carRestTemplate = carRestTemplate;
    }

    public List<Car> getCarsWithColor(Color color) {
        return Arrays.asList(carRestTemplate.getForObject("/cars?colorName=" + color, Car[].class));
    }
}
