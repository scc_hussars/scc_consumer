package com.capgemini.scc.consumer.model;

public enum Color {
    RED, GREEN, BLUE, BLACK, WHITE
}
