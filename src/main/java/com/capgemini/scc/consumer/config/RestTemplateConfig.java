package com.capgemini.scc.consumer.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestTemplateConfig {

    @Value("${producer.port}")
    public int producerPort;

    @Bean
    @Autowired
    public RestTemplate carRestTemplate(RestTemplateBuilder builder) {
        return builder.rootUri("http://localhost:" + producerPort)
                .build();
    }
}
